from django.urls import path
from django.shortcuts import render
import requests

url = "http://52.47.85.87:8100/"
gate = "2"


def GateUpload(request):
    if request.method == "POST":
        file = {"IMG": ("img.png", request.FILES.get("photo"))}
        re = requests.post(url + "rpi/upload", files=file).json()
        path = re["IMG"]
        gate_id = gate
        data = {"path": path, "gate": gate_id}
        re = requests.get(url + "rip/compare", data=data).json()

        if re["message"] =='grant':
            print("oppening gate")

        return render(request, "gate.html", {"message": re["message"]})
    return render(request, "gate.html")


urlpatterns = [path("", GateUpload, name="upload")]
